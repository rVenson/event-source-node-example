const express = require('express')
const app = express()
const cors = require('cors')

app.use(cors())
app.use(express.json())
app.use(express.urlencoded())
app.use(express.static('./public'))

app.get('/events', (req, res, next) => {
    try{
        console.log("client started")

        const headers = {
            'Content-Type': 'text/event-stream',
            'Connection': 'keep-alive',
            'Cache-Control': 'no-cache'
        }
        res.writeHead(200, headers)

        // NOTA: É necessário adicionar o atributo 'data' e as quebras de linha '\n\n' para que
        // isso seja entendido pelo EventSource como um evento 'message'
        res.write(`data:teste\n\n`)

        req.on('close', () => {
            console.log(`Client Connection closed`);
            clients = clients.filter(item => item.id !== client.id);
        })

        setTimeout(() => {
            console.log("enviando mensagem para " + client.id)
            res.write(`data:Tchau cliente!\n\n`)
            res.write(`data:end\n\n`)
        }, 3000)
    } catch (error){
        next(error)
    }
})

app.listen(8080, function(){
    console.log('server running')
})